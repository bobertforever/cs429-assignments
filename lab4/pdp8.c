/* 
 * pdp8.c
 * Robert Lynch
 * rml953
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef short Boolean;
#define TRUE 1
#define FALSE 0

// Decode input file
FILE *file;

// FDE cycle
Boolean ok = TRUE;
int pc;
int curtime = 0;

// data for the virtual machine
unsigned int memory[4096];
int accumulator;
char link;

// Data for verbose mode
Boolean verbose = FALSE;
Boolean opI = FALSE;
char opname[50];

int getmemloc(int inst) {
    Boolean indirect = (inst >> 8) & 1;
    Boolean zeropage = (inst >> 7) & 1;

    int page = zeropage ? (pc * 32 / 4096) : 0;

    int tmploc = ((inst & 0x07F) + (page * 128));
    if(indirect) {
        curtime++;
        opI = TRUE;
        return memory[tmploc];
    } else {
        opI = FALSE;
        return tmploc;
    }
}

void fliplink(void) {
    if(link) {
        link = 0;
    } else {
        link = 1;
    }
}

void and(int addr) {
    accumulator = memory[addr] & accumulator;
    curtime += 2;
    pc++;
}

void tad(int addr) {
    accumulator = memory[addr] + accumulator;

    if(accumulator >> 12) {
        fliplink();
        accumulator &= 0xFFF;
    }

    curtime += 2;
    pc++;
}

void isz(int addr) {
    memory[addr]++;
    curtime += 2;

    if(memory[addr] >> 12)
        memory[addr] = 0;

    if(memory[addr] == 0) {
        pc += 2;
    } else {
        pc++;
    }
}

void dca(int addr) {
    memory[addr] = accumulator;
    accumulator = 0;
    curtime += 2;
    pc++;
}

void jms(int addr) {
    memory[addr] = pc + 1;
    pc = addr + 1;
    curtime += 2;
}

void jmp(int addr) {
    pc = addr;
    curtime++;
}

void iot(void) {
    int device = (memory[pc] >> 3) & 0x3F;
    if(device != 3 && device != 4) {
        fprintf(stderr, "Error at %i: IOT device is invalid (%i)\n", pc, device);
        exit(1);
    }

    if(device == 4) {
        putchar(accumulator & 0xFF);
    } else {
        accumulator = getchar();
        accumulator &= 0xFFF;
    }
    pc++;
    curtime++;
}

void operate(void) {
    int inst = memory[pc];
    Boolean group2 = (inst >> 8) & 1;

    if(!group2) {
        // Check for error
        if((inst >> 2 & 1) && (inst >> 3 & 1)) {
            fprintf(stderr, "Error at %i: both RAR and RAL are set\n", pc);
            exit(1);
        }
        // CLA
        if(inst >> 7 & 1)
            accumulator = 0;
        // CLL
        if(inst >> 6 & 1)
            link = 0;
        // CMA
        if(inst >> 5 & 1)
            accumulator = (~accumulator & 0xFFF);
        // CML
        if(inst >> 4 & 1)
            fliplink();
        // IAC
        if(inst & 1) {
            accumulator++;
            if(accumulator >> 12) {
                fliplink();
                accumulator = accumulator & 0x0FFF;
            }
        }
        // RAR
        if(inst >> 3 & 1) {
            link = accumulator & 1;
            accumulator = accumulator >> 1;
            accumulator |= (link << 11);
            if(inst >> 1 & 1) {
                link = accumulator & 1;
                accumulator >> 1;
                accumulator |= (link << 11);
            }
        }
        // RAL
        if(inst >> 2 & 1) {
            link = (accumulator >> 11) & 1;
            accumulator = accumulator << 1;
            accumulator |= link;
            if(inst >> 1 & 1) {
                link = (accumulator >> 11) & 1;
                accumulator = accumulator << 1;
                accumulator |= link;
            }
        }
    } else {
        // Check for error
        if(inst & 1) {
            fprintf(stderr, "Error at %i: operate group 2 low order bit set\n", pc);
            exit(1);
        }

        Boolean rss = inst >> 3 & 1;
 
        // SMA
        if(inst >> 6 & 1) {
            if(rss) {
                if((accumulator << ((sizeof(int) * 8) - 12)) >= 0)
                    pc++;
            } else {
                if((accumulator << ((sizeof(int) * 8) - 12)) < 0)
                    pc++;
            }
        }
        // SZA
        if(inst >> 5 & 1) {
            if(rss) {
                if(accumulator != 0)
                    pc++;
            } else {
                if(accumulator == 0)
                    pc++;
            }
        }
        // SNL
        if(inst >> 4 & 1) {
            if(rss) {
                if(link != 1)
                    pc++;
            } else {
                if(link == 1)
                    pc++;
            }
        }
        // CLA
        if(inst >> 7 & 1)
            accumulator = 0;
        // OSR
        if(inst >> 2 & 1) {
            //noop
        }
        // HLT
        if(inst >> 1 & 1)
            ok = FALSE;
    }
    pc++;
    curtime++;
}

void getoperateopname(int curpc) {
    int inst = memory[curpc];
    Boolean group2 = (inst >> 8) & 1;

    strcpy(opname, "");
    if(!group2) {
        if(inst >> 7 & 1)
            strcat(opname, "CLA ");
        if(inst >> 6 & 1)
            strcat(opname, "CLL ");
        if(inst >> 5 & 1)
            strcat(opname, "CMA ");
        if(inst >> 4 & 1)
            strcat(opname, "CML ");
        if(inst & 1)
            strcat(opname, "IAC ");
        if(inst >> 1 & 1) {
            if(inst >> 3 & 1)
                strcat(opname, "RTR ");
            if(inst >> 2 & 1)
                strcat(opname, "RTL ");
        } else {
            if(inst >> 3 & 1)
                strcat(opname, "RAR ");
            if(inst >> 2 & 1)
                strcat(opname, "RAL ");
        }
    } else {
        if(inst >> 6 & 1)
            strcat(opname, "SMA ");
        if(inst >> 5 & 1)
            strcat(opname, "SZA ");
        if(inst >> 4 & 1)
            strcat(opname, "SNL ");
        if(inst >> 3 & 1)
            strcat(opname, "RSS ");
        if(inst >> 7 & 1)
            strcat(opname, "CLA ");
        if(inst >> 2 & 1)
            strcat(opname, "OSR ");
        if(inst >> 1 & 1)
            strcat(opname, "HLT ");
    }

    if(opname[strlen(opname) - 1] == ' ')
        opname[strlen(opname) - 1] = '\0';
}

void updateopname(int curpc) {
    int op = memory[curpc] >> 9;
    char buffer[6];
    
    switch(op) {
        case 0:
            strcpy(opname, "AND");
            break;
        case 1:
            strcpy(opname, "TAD");
            break;
        case 2:
            strcpy(opname, "ISZ");
            break;
        case 3:
            strcpy(opname, "DCA");
            break;
        case 4:
            strcpy(opname, "JMS");
            break;
        case 5:
            strcpy(opname, "JMP");
            break;
        case 6:
            sprintf(opname, "IOT %i", (memory[curpc] >> 3) & 0x03F);
            opI = FALSE;
            break;
        case 7:
            getoperateopname(curpc);
            opI = FALSE;
            break;
    }

    if(opI) {
        strcat(opname, " I");
    }
}

void process(void) {
    // Variables to hold the line of data read in
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    char *memloc = "000";
    char *inst = "000";

    // Read the object file into emmulated memory
    while ((read = getline(&line, &len, file)) != -1) {
        memloc = strtok(line, ": ");
        inst = strtok(NULL, "\n");

        if(!memloc || !inst) {
            fprintf(stderr, "Invalid object file.\n");
            exit(1);
        }

        // Clear leading whitespace from instruction
        const char* trim = inst;
        while(*trim != '\0' && isspace(*trim))
            ++trim;

        size_t len = strlen(trim)+1;         
        memmove(inst, trim, len);

        if(strcmp(memloc, "EP") == 0) {
            pc = (int) strtol(inst, NULL, 16);
        } else {
            memory[strtol(memloc, NULL, 16)] = strtol(inst, NULL, 16);
        }
    }

    // Do the FDE cycle
    while(ok) {
        int op = memory[pc] >> 9;
        int tmppc = pc;
        
        int addr;
        if(op < 6)
            addr = getmemloc(memory[pc]);

        switch(op) {
            case 0:
                and(addr);
                break;
            case 1:
                tad(addr);
                break;
            case 2:
                isz(addr);
                break;
            case 3:
                dca(addr);
                break;
            case 4:
                jms(addr);
                break;
            case 5:
                jmp(addr);
                break;
            case 6:
                iot();
                break;
            case 7:
                operate();
                break;
        }

        if(pc >> 12) {
            pc &= 0xFFF;
        }

        if(verbose) {
            updateopname(tmppc);
            fprintf(stderr, "Time %i: PC=0x%03X instruction = 0x%03X (%s), rA = 0x%03X, rL = %i\n", curtime, tmppc, memory[tmppc], opname, accumulator, link);
        }
    }
}

void scanargs(char *s) {
    while (*s != '\0') {
        switch (*s++) {
            case '-':
                break;

            case 'v':
                verbose = TRUE;
                break;

            default:
                fprintf(stderr, "php8: Bad option %c\n", s[-1]);
                fprintf(stderr, "       -v -- verbose mode\n");
                exit(1);
        }
    }
}

int main (int argc, char **argv) {
    // Read args and open object file
    while (argc > 1) {
        argc--, argv++;
        if (**argv == '-') {
            scanargs(*argv);
        } else {
            file = fopen(*argv, "r");
            if (!file) {
                fprintf(stderr, "pdp8: Cannot open file %s\n", *argv);
                exit(1);
            }
        }
    }

    process();

    if(file)
        fclose(file);

    return 0;
}
