#line 169 "bits.c"
int logicalShift(int x, int n) {
  return (x >> n) & ~(((1 << 31) >> n) << 1);
}
#line 180
int bitOr(int x, int y) {
  return ~(~x & ~y);
}
#line 191
int isPositive(int x) {

  int sign=  x >> 31;

  return !(sign) & !(!x);
}
#line 205
int allOddBits(int x) {

  int mask=  0xAA;
  mask |=( 0xAA << 8);
  mask |=( 0xAA << 16);
  mask |=( 0xAA << 24);

  return !(~x & mask);
}
#line 223
int absVal(int x) {

  int sign=  x >> 31;

  return (sign ^ x) + ~sign + 1;
}
#line 238
int getByte(int x, int n) {
  return (x >>( n << 3)) & 0xFF;
}
#line 250
int divpwr2(int x, int n) {
  return (x +(( x >> 31) &(( 1 << n) + ~0))) >> n;
}
#line 261
int bitParity(int x) {
  x ^= x >> 16;
  x ^= x >> 8;
  x ^= x >> 4;
  x ^= x >> 2;
  x ^= x >> 1;
  return x & 1;
}
#line 281
unsigned float_twice(unsigned uf) {
  int exp=(  uf >> 23) & 0xFF;
  int sign=  uf & 0x80000000;
  int frac=  uf & 0x007FFFFF;


  if (exp == 255 ||( exp == 0 && frac == 0)) 
      return uf;

  if (exp) {

    exp++;
  } else if (frac == 0x7FFFFF) {

    frac = frac - 1;
    exp = exp + 1;
  } else {

    frac = frac << 1;
  }


  return (sign) |( exp << 23) |( frac);
}
#line 318
int float_f2i(unsigned uf) {
  return 2;
}
#line 334
int howManyBits(int x) {
  return 0;
}
