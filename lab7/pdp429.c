/*
  PDP-429 Interpreter:

   switch statements be here
*/

/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

FILE *input;

typedef short Boolean;
#define TRUE 1
#define FALSE 0

Boolean debug = FALSE;
Boolean verbose = FALSE;
Boolean ASCII = FALSE;

typedef char *STRING;

#define CAST(t,e) ((t)(e))
#define TYPED_MALLOC(t) CAST(t*, malloc(sizeof(t)))


/* ***************************************************************** */
/*                                                                   */
/* print representation of a character for debugging                 */
/*                                                                   */
char   *printrep (unsigned short  c)
{
    static char pr[8];

    if (c < 32)
        {
            /* control characters */
            pr[0] = '^';
            pr[1] = c + 64;
            pr[2] = '\0';
        }
    else if (c < 127)
        {
            /* printing characters */
            pr[0] = c;
            pr[1] = '\0';
        }
    else if (c == 127)
        return("<del>");
    else if (c <= 0377)
        {
            /* upper 128 codes from 128 to 255;  print as \ooo - octal  */
            pr[0] = '\\';
            pr[3] = '0' + (c & 7);
            c = c >> 3;
            pr[2] = '0' + (c & 7);
            c = c >> 3;
            pr[1] = '0' + (c & 3);
            pr[4] = '\0';
        }
    else
        {
            /* very large number -- print as 0xffff - 4 digit hex */
            (void)sprintf(pr, "0x%04x", c);
        }
    return(pr);
}


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

/* MALLOC space for a string and copy it */

STRING remember_string(const STRING name)
{
    size_t n;
    STRING p;

    if (name == NULL) return(NULL);

    /* get memory to remember file name */
    n = strlen(name) + 1;
    p = CAST(STRING, malloc(n));
    strcpy(p, name);
    return(p);
}


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

char line[10];
int line_length = 8;

int get_next_line(void)
{
    /* get the first character to see if we have EOF */
    int c;
    int i = 0;
    c = getc(input);
    if (c != EOF)
        {
            line[i] = c;
            i = 1;
            while (((c = getc(input)) != EOF) && (c != '\n'))
                {
                    if (i < line_length)
                        {
                            line[i] = c;
                            i = i + 1;
                        }
                }
        }
    line[i] = '\0';

    if (debug) fprintf(stderr, "next input line: %s\n", line);

    if ((c == EOF) && (i == 0))
        return(EOF);
    else
        return(i);
}



/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

Boolean is_hex(char c)
{
    if (('0' <= c) && (c <= '9')) return(TRUE);
    if (('A' <= c) && (c <= 'F')) return(TRUE);
    if (('a' <= c) && (c <= 'f')) return(TRUE);
    return(FALSE);
}

int hex_char_value(char c)
{
    if (('0' <= c) && (c <= '9')) return(c-'0');
    if (('A' <= c) && (c <= 'F')) return(c-'A' + 10);
    if (('a' <= c) && (c <= 'f')) return(c-'a' + 10);
    return(-1);
}

int hex_value(char *p)
{
    int n = 0;
    while (is_hex(*p))
        {
            n = n * 16 + hex_char_value(*p);
            p++;
        }
    return(n);
}


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

/* PDP-8 types */

typedef int Address;
typedef int INST;
typedef int Word;


#define MASK_W16      0xFFFF
#define MASK_SIGN_BIT 0x8000

#define MASK_I_BIT   0x0200
#define MASK_ZC_BIT  0x0100
#define MASK_OFFSET  0x00FF

#define MASK_OVERFLOW 0xF0000    /* Any bit in high-order 4 bits is overflow */

#define MASK_SM     0x0200
#define MASK_SZ     0x0100
#define MASK_SNL    0x0080
#define MASK_RSS    0x0040
#define MASK_CL     0x0020
#define MASK_CLL    0x0010
#define MASK_CM     0x0008
#define MASK_CML    0x0004
#define MASK_DC     0x0002
#define MASK_IN     0x0001

#define MASK_IOT_DEVICE   0x03F8
#define MASK_IOT_FUNCTION 0x0007


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

INST     memory[65536];
Boolean defined[65536];


void Clear_Memory(void)
{
    int i;
    for (i = 0; i < 65536; i++)
        {
            defined[i] = FALSE;
        }
}

void Store_Memory(Address addr, Word value)
{
    if (debug)
        fprintf(stderr, "write memory: 0x%03X = 0x%03X\n", addr, value);
    defined[addr] = TRUE;
    value = value & MASK_W16;
    memory[addr] = value;
}

INST Fetch_Memory(Address addr)
{
    Word value;

    if (defined[addr])
        value = memory[addr];
    else
        value = 0;

    if (debug)
        fprintf(stderr, "read memory: 0x%03X = 0x%03X\n", addr, value);
    return(value);
}



/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

/* registers */
Word A = 0;
Word B = 0;
Word C = 0;
Word D = 0;

Address PC = 0;
Word PSW = 0;
Word SP = 0;
Word SPL = 0;

Word L = 0;

int *get_register(int i)
{
    switch(i)
    {
    case 0:
        return &A;
    case 1:
        return &B;
    case 2:
        return &C;
    case 3:
        return &D;
    case 4:
        return &PC;
    case 5:
        return &PSW;
    case 6:
        return &SP;
    case 7:
        return &SPL;
    }

    return &A;
}

/* internal controls */
Word Switch_Register = 0;
Boolean Halted = TRUE;
long long time = 0;



/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

Address Load_ASCII_Object_File(STRING name)
{
    Address entry_point = 0;
    Word data;

    while (get_next_line() != EOF)
        {
            char *p = line;
            while (isalnum(*p)) p++;
            *p = '\0';
            while (!is_hex(*p)) p++;
            /* two values: one at line, the other at p */
            data = hex_value(p);
            if (strcasecmp(line, "EP") == 0)
                entry_point = data;
            else
                {
                    Address addr = hex_value(line);
                    Store_Memory(addr, data);
                }
        }

    return(entry_point);
}

/* ***************************************************************** */
/*                                                                   */
/* ***************************************************************** */

int get2(void)
{
    int c1 = getc(input);
    int c2 = getc(input);
    if (debug) fprintf(stderr, "read two bytes: 0x%X, 0x%X\n", c1, c2);
    if ((c1 == EOF) || (c2 == EOF))
        {
            fprintf(stderr, "Premature EOF\n");
            exit(1);
        }
    if (c1 & (~0xFF)) fprintf(stderr, "Extra high order bits for 0x%X\n", c1);
    if (c2 & (~0xFF)) fprintf(stderr, "Extra high order bits for 0x%X\n", c2);
    int n = ((c1 & 0xFF) << 8) | (c2 & 0xFF);
    return(n);
}

Address Load_Binary_Object_File(STRING name)
{
    int c1 = getc(input);
    int c2 = getc(input);
    int c3 = getc(input);
    int c4 = getc(input);
    if (debug) fprintf(stderr, "read four bytes: 0x%X, 0x%X, 0x%X, 0x%X\n", c1, c2, c3, c4);

    if ((c1 != 'O') || (c2 != 'B') || (c3 != 'J') || (c4 != 'G'))
        {
            fprintf(stdout, "First four bytes are not OBJ8: ");
            fprintf(stdout, "%s", printrep(c1));
            fprintf(stdout, "%s", printrep(c2));
            fprintf(stdout, "%s", printrep(c3));
            fprintf(stdout, "%s", printrep(c4));
            fprintf(stdout, " (%02X %02X %02X %02X)\n", c1, c2, c3, c4);

            exit(1);
        }

    Address entry_point = get2();

    int n;
    while ((n = getc(input)) != EOF)
        {
            if (debug) fprintf(stderr, "Read next block of %d bytes\n", n);
            n = n - 1;
            Address addr = get2(); n -= 2;
            while (n > 0)
                {
                    Word data = get2(); n -= 2;            
                    Store_Memory(addr, data);
                    addr += 1;
                }
        }

    return(entry_point);
}

void Load_Object_File(STRING name)
{
    Address entry_point = 0;

    Clear_Memory();

    if (ASCII)
        entry_point = Load_ASCII_Object_File(name);
    else
        entry_point = Load_Binary_Object_File(name);

    time = 0;
    Halted = FALSE;
    PC = entry_point & MASK_W16;
}


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

/* constructing the opcode name for an instruction */

char opcode_name[32];

void clear_opcode(void)
{
    opcode_name[0] = '\0';
}

void append_opcode(STRING name)
{
    if (opcode_name[0] != '\0')
        strncat(opcode_name, " ", sizeof(opcode_name));
    strncat(opcode_name, name, sizeof(opcode_name));
}

char *get_opcode(void)
{
    return(opcode_name);
}

/* constructing the instruction description */

char instruction_description[100];

void clear_instruction_description(void)
{
    instruction_description[0] = '\0';
}

void append_instruction_description(STRING inst)
{
    if(instruction_description[0] != '\0')
        strncat(instruction_description, ", ", sizeof(instruction_description));
    else
        strncat(instruction_description, ": ", sizeof(instruction_description));
    strncat(instruction_description, inst, sizeof(instruction_description));
}

char *get_instruction_description(void)
{
    return instruction_description;
}

char *get_register_name(int i)
{
    switch(i)
        {
        case 0: return "A";
        case 1: return "B";
        case 2: return "C";
        case 3: return "D";
        case 4: return "PC";
        case 5: return "PSW";
        case 6: return "SP";
        case 7: return "SPL";
        }
    return "<bad-register>";
}

/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

void push_stack(Word value)
{
    char buffer[30];

    if(SP < SPL) 
    {
        append_opcode("Stack Overflow");

        fprintf(stderr, "Stack Pointer = 0x%04X; Stack Limit = 0x%04X\n", SP, SPL);
        sprintf(buffer, "PSW -> 0x%04X", PSW);
        append_instruction_description(buffer);
        time = time - 1;

        Halted = TRUE;
        PSW &= 0xFFFE;
        
        sprintf(buffer, "0x%04X -> PSW", PSW);
        append_instruction_description(buffer);
        return;
    }

    sprintf(buffer, "0x%04X -> M[0x%04X]", value, SP);
    append_instruction_description(buffer);

    Store_Memory(SP, value);
    SP = SP - 1;
    
    sprintf(buffer, "0x%04X -> SP", SP);
    append_instruction_description(buffer);
}

INST pop_stack(void) 
{
    char buffer[30];

    if(SP == 0xFFFF)
    {
        append_opcode("Stack Overflow");

        fprintf(stderr, "Stack Pointer overflow; halting\n");
        sprintf(buffer, "PSW -> 0x%04X", PSW);
        append_instruction_description(buffer);
        time = time - 1;

        Halted = TRUE;
        PSW &= 0xFFFE;
        
        sprintf(buffer, "0x%04X -> PSW", PSW);
        append_instruction_description(buffer);
        return 0;
    }

    sprintf(buffer, "SP -> 0x%04X, 0x%04X -> SP", SP, SP + 1);
    append_instruction_description(buffer);

    SP = SP + 1;
    INST inst = Fetch_Memory(SP);

    sprintf(buffer, "M[0x%04X] -> 0x%04X", SP, inst);
    append_instruction_description(buffer);

    return inst;
}


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

void Check_Overflow(Word *reg)
{
    /* check for overflow and complement L if so */
    if ((*reg & MASK_OVERFLOW) != 0) 
        {
            L = 1;
            char buffer[20];
            sprintf(buffer, "0x%04X -> L", L);
            append_instruction_description(buffer);
        }
    *reg = *reg & MASK_W16;
}

void Valid_Sign(Word a, Word b, Word c)
{
    if((a & MASK_SIGN_BIT) == (b & MASK_SIGN_BIT))
        {
            if((c & MASK_SIGN_BIT) != (a & MASK_SIGN_BIT))
                {
                    L = 1;
                    char buffer[20];
                    sprintf(buffer, "0x%04X -> L", L);
                    append_instruction_description(buffer);
                }
        }
}

int Decode_Instruction(INST inst)
{
    return((inst >> 12) & 0xF);
}

Address Memory_Reference_Address(Address old_PC, INST inst)
{
    /* get the addr */
    Address addr = inst & MASK_OFFSET;
    /* check for Z/C = 1 -> current page */
    if ((inst & MASK_ZC_BIT) != 0)
        addr = addr | (old_PC & ~MASK_OFFSET);
    /* check for I/D = 1 -> indirect */
    if ((inst & MASK_I_BIT) != 0)
        {
            append_opcode("I");
            char buffer[25];
            Address temp = addr;
            addr = Fetch_Memory(addr);
            sprintf(buffer, "M[0x%04X] -> 0x%04X", temp, addr);
            append_instruction_description(buffer);
            time = time + 1;
        }
    return(addr);
}

void divide(Word *reg, Word i, Word j)
{
    if((i & MASK_SIGN_BIT) && !(j & MASK_SIGN_BIT))
        {
            i = (~i & MASK_W16) + 1;
            *reg = i / j;
            *reg = (~*reg & MASK_W16) + 1;
        }
    else if (!(i & MASK_SIGN_BIT) && (j & MASK_SIGN_BIT))
        {
            j = (~j & MASK_W16) + 1;
            *reg = i / j;
            *reg = (~*reg & MASK_W16) + 1;
        }
    else if ((i & MASK_SIGN_BIT) && (j & MASK_SIGN_BIT))
        {
            i = (~i & MASK_W16) + 1;
            j = (~j & MASK_W16) + 1;
            *reg = i / j;
        }
    else
        {
            *reg = i / j;
        }
}

void mod(Word *reg, Word i, Word j)
{
    if((i & MASK_SIGN_BIT) && !(j & MASK_SIGN_BIT))
        {
            i = (~i & MASK_W16) + 1;
            *reg = i % j;
            *reg = (~*reg & MASK_W16) + 1;
        }
    else if (!(i & MASK_SIGN_BIT) && (j & MASK_SIGN_BIT))
        {
            j = (~j & MASK_W16) + 1;
            *reg = i % j;
            *reg = (~*reg & MASK_W16) + 1;
        }
    else if ((i & MASK_SIGN_BIT) && (j & MASK_SIGN_BIT))
        {
            i = (~i & MASK_W16) + 1;
            j = (~j & MASK_W16) + 1;
            *reg = i % j;
        }
    else
        {
            *reg = i % j;
        }

    *reg = *reg & MASK_W16;
}

void non_register_non_memory_instruction(INST inst)
{
    char buffer[20];

    switch(inst)
        {
        case 0: /* NOP */
            append_opcode("NOP");
            break;
        case 1: /* HLT */
            append_opcode("HLT");            
            sprintf(buffer, "PSW -> 0x%04X", PSW);
            append_instruction_description(buffer);

            Halted = TRUE;
            PSW &= 0xFFFE;
            
            sprintf(buffer, "0x%04X -> PSW", PSW);
            append_instruction_description(buffer);
            break;
        case 2: /* RET */
            append_opcode("RET");
            
            PC = pop_stack();
            time = time + 1;

            sprintf(buffer, "0x%04X -> PC", PC);
            append_instruction_description(buffer);
            break;
        }
}

void register_memory_reference_instructions(INST inst, int opcode, int old_PC)
{
    Address addr = Memory_Reference_Address(old_PC, inst);
    Word value = Fetch_Memory(addr);
    Word *reg = get_register((inst >> 10) & 0x3);
    Word temp;
    char *regc = get_register_name((inst >> 10) & 0x3);
    char buffer[100];

    if(opcode <= 7)
        {
            sprintf(buffer, "%s -> 0x%04X, M[0x%04X] -> 0x%04X", regc, *reg, addr, value);
            append_instruction_description(buffer);
        }

    switch(opcode)
        {
        case 1: /* ADD */
            sprintf(buffer, "ADD%s", regc);
            append_opcode(buffer);
            temp = *reg;
            *reg = *reg + value;
            Check_Overflow(reg);
            Valid_Sign(temp, value, *reg);
            break;

        case 2: /* SUB */
            sprintf(buffer, "SUB%s", regc);
            append_opcode(buffer);
            *reg = (*reg - value) & MASK_W16;
            Check_Overflow(reg);
            break;

        case 3: /* MUL */
            sprintf(buffer, "MUL%s", regc);
            append_opcode(buffer);
            temp = *reg;
            *reg = *reg * value;
            Check_Overflow(reg);
            Valid_Sign(temp, value, *reg);
            break;

        case 4: /* DIV */
            sprintf(buffer, "DIV%s", regc);
            append_opcode(buffer);
            if(value == 0)
                {
                    L = 1;
                    sprintf(buffer, "0x%04X -> L", L);
                    append_instruction_description(buffer);
                    *reg = 0;
                    break;
                }
            divide(reg, *reg, value);
            break;

        case 5: /* AND -- Bitwise and */
            sprintf(buffer, "AND%s", regc);
            append_opcode(buffer);
            *reg = *reg & value;
            break;

        case 6: /* OR -- Bitwise or */
            sprintf(buffer, "OR%s", regc);
            append_opcode(buffer);
            *reg = *reg | value;
            break;

        case 7: /* XOR -- Bitwise xor*/
            sprintf(buffer, "XOR%s", regc);
            append_opcode(buffer);
            *reg = *reg ^ value;
            break;

        case 8: /* LD -- load from memory */
            sprintf(buffer, "LD%s", regc);
            append_opcode(buffer);
            *reg = value;
            break;
        
        case 9: /* ST -- Store into memory */
            sprintf(buffer, "ST%s", regc);
            append_opcode(buffer);
            Store_Memory(addr, *reg);
            break;
        }

    if(opcode <= 7)
        sprintf(buffer, "0x%04X -> %s", *reg, regc);          
    else if (opcode == 8)
        sprintf(buffer, "M[0x%04X] -> 0x%04X, 0x%04X -> %s", addr, *reg, *reg, regc);
    else
        sprintf(buffer, "%s -> 0x%04X, 0x%04X -> M[0x%04X]", regc, *reg, *reg, addr);

    append_instruction_description(buffer);
}

void non_register_memory_reference_instruction(Address old_PC, INST inst)
{
    int subopcode = (inst >> 10) & 0x3;

    Address addr = Memory_Reference_Address(old_PC, inst);
    Word value = Fetch_Memory(addr);

    char buffer[20];

    switch(subopcode)
        {
        case 0: /* ISZ */
            append_opcode("ISZ");
            sprintf(buffer, "M[0x%04X] -> 0x%04X", addr, value);
            append_instruction_description(buffer);

            value = (value + 1) & MASK_W16;
            Store_Memory(addr, value);
            time = time + 2;
            
            sprintf(buffer, "0x%04X -> M[0x%04X]", value, addr);
            append_instruction_description(buffer);
            
            if (value == 0)
                {
                    PC = (PC + 1) & MASK_W16;
                    sprintf(buffer, "0x%04X -> PC", PC);
                    append_instruction_description(buffer);
                }
            break;

        case 1: /* JMP */
            append_opcode("JMP");
            
            PC = addr;

            sprintf(buffer, "0x%04X -> PC", PC);
            append_instruction_description(buffer);
            break;

        case 2: /* CALL */
            append_opcode("CALL");
            
            push_stack((old_PC + 1) & 0xFFFF);
            PC = addr;
            time = time + 1;

            sprintf(buffer, "0x%04X -> PC", PC);
            append_instruction_description(buffer);
            break;
        }
}

void stack_operation_instruction(int old_PC, INST inst)
{
    Address addr = Memory_Reference_Address(old_PC, inst);
    Word value = Fetch_Memory(addr);

    int subopcode = (inst >> 10) & 0x3;

    char buffer[20];

    if (subopcode == 0)
        {
            append_opcode("PUSH");
            sprintf(buffer, "M[0x%04X] -> 0x%04X", addr, value);
            append_instruction_description(buffer);
            push_stack(value);
        }
    else if (subopcode == 1)
        {
            append_opcode("POP");
            INST stackV = pop_stack();
            Store_Memory(addr, stackV);

            sprintf(buffer, "0x%04X -> M[0x%04X]", stackV, addr);
            append_instruction_description(buffer);
        }
    else
        {
            append_opcode("<bad-instruction>");
            fprintf(stderr, "Subopcode for stack operations invalid; halting\n");
            Halted = TRUE;
        }
}

void register_to_register_instruction(INST inst)
{
    Word *regi = get_register((inst >> 6) & 0x7);
    Word *regj = get_register((inst >> 3) & 0x7);
    Word *regk = get_register(inst & 0x7);

    int subopcode = (inst >> 9) & 0x7;

    
    char buffer[50];
    char *regic = get_register_name((inst >> 6) & 0x7);
    char *regjc = get_register_name((inst >> 3) & 0x7);
    char *regkc = get_register_name(inst & 0x7);
    sprintf(buffer, "%s -> 0x%04X, %s -> 0x%04X", regjc, *regj, regkc, *regk);
    append_instruction_description(buffer);

    switch(subopcode)
        {
        case 0: /* MOD */
            append_opcode("MOD");
            if(*regk == 0)
                {
                    L = 1;
                    sprintf(buffer, "0x%04X -> L", L);
                    append_instruction_description(buffer);
                    *regi = 0;
                    break;
                }
            mod(regi, *regj, *regk);
            break;

        case 1: /* ADD */
            append_opcode("ADD");
            *regi = *regj + *regk;
            Valid_Sign(*regj, *regk, *regi);
            break;

        case 2: /* SUB */
            append_opcode("SUB");
            *regi = (*regj - *regk) & MASK_W16;
            break;

        case 3: /* MUL */
            append_opcode("MUL");
            *regi = *regj * *regk;
            Valid_Sign(*regj, *regk, *regi); 
            break;

        case 4: /* DIV */
            append_opcode("DIV");    
            if(*regk == 0)
                {
                    L = 1;
                    sprintf(buffer, "0x%04X -> L", L);
                    append_instruction_description(buffer);
                    *regi = 0;
                    break;
                }
            divide(regi, *regj, *regk);
            break;

        case 5: /* AND */
            append_opcode("AND");
            *regi = *regj & *regk;
            break;

        case 6: /* OR */
            append_opcode("OR");
            *regi = *regj | *regk;
            break;

        case 7: /* XOR */
            append_opcode("XOR");
            *regi = *regj ^ *regk;
            break;
        }

    Check_Overflow(regi);

    sprintf(buffer, "0x%04X -> %s", *regi, regic);
    append_instruction_description(buffer);
}

void non_memory_register_instruction(INST inst)
{
    Word *reg = get_register((inst >> 10) & 0x3);
    Boolean skip = FALSE;

    char *regc = get_register_name((inst >> 10) & 0x3);
    char buffer[20];
    Boolean print = TRUE;

    /* SM - Skip if the register is negative */
    if(inst & MASK_SM)
        {
            sprintf(buffer, "SM%s", regc);
            append_opcode(buffer);
            skip = skip || (*reg & MASK_SIGN_BIT) != 0;

            if(print)
                {
                    sprintf(buffer, "%s -> 0x%04X", regc, *reg);
                    append_instruction_description(buffer);
                    print = !skip;
                }
        }

    /* SZ - Skip if the register is zero */
    if(inst & MASK_SZ)
        {
            sprintf(buffer, "SZ%s", regc);
            append_opcode(buffer);
            skip = skip || (*reg == 0);

            if(print)
                {
                    sprintf(buffer, "%s -> 0x%04X", regc, *reg);
                    append_instruction_description(buffer);
                    print = !skip;
                }     
        }

    /* SNL - Skip if the Link bit is non-zero */
    if(inst & MASK_SNL)
        {
            append_opcode("SNL");
            skip = skip || (L != 0);

            if(print)
                {
                    sprintf(buffer, "L -> 0x%04X", L);
                    append_instruction_description(buffer);
                    print = !skip;
                }
        }

    /* RSS - Reverse the Skip Sense */
    if(inst & MASK_RSS)
        {
            append_opcode("RSS");
            skip = !skip;
        }

    if(skip)
        {
            PC = (PC + 1) & MASK_W16;
            sprintf(buffer, "0x%04X -> PC", PC);
            append_instruction_description(buffer);
        } 
        
    /* CL - Clear the register */
    if(inst & MASK_CL)
        {
            sprintf(buffer, "CL%s", regc);
            append_opcode(buffer);
            sprintf(buffer, "0x0000 -> %s", regc);
            append_instruction_description(buffer);
            *reg = 0;
        }

    /* CLL - Clear the Link bit */
    if(inst & MASK_CLL)
        {
            append_opcode("CLL");
            append_instruction_description("0x0000 -> L");
            L = 0;
        }

    /* CM - Complement the register */
    if(inst & MASK_CM)
        {
            sprintf(buffer, "CM%s", regc);
            append_opcode(buffer);
            sprintf(buffer, "%s -> 0x%04X", regc, *reg);
            append_instruction_description(buffer);

            *reg = (~(*reg)) & MASK_W16;

            sprintf(buffer, "0x%04X -> %s", *reg, regc);
            append_instruction_description(buffer);
        }

    /* CML - Complement the Link bit */
    if(inst & MASK_CML)
        {
            append_opcode("CML");
            sprintf(buffer, "L -> 0x%04X", L);
            append_instruction_description(buffer);

            L = 1 - L;

            sprintf(buffer, "0x%04X -> L", L);
            append_instruction_description(buffer);
        }

    /* DC - Decrement the register by one */
    if(inst & MASK_DC)
        {
            sprintf(buffer, "DC%s", regc);
            append_opcode(buffer);
            sprintf(buffer, "%s -> 0x%04X", regc, *reg);
            append_instruction_description(buffer);
            
            *reg = (*reg - 1) & MASK_W16;

            sprintf(buffer, "0x%04X -> %s", *reg, regc);
            append_instruction_description(buffer);
        }

    /* IN - Increment the register by one */
    if(inst & MASK_IN)
        {
            sprintf(buffer, "IN%s", regc);
            append_opcode(buffer);
            sprintf(buffer, "%s -> 0x%04X", regc, *reg);
            append_instruction_description(buffer);
            
            Word temp = *reg;
            *reg = (*reg + 1) & MASK_W16;
            Valid_Sign(temp, 1, *reg);
            
            sprintf(buffer, "0x%04X -> %s", *reg, regc);
            append_instruction_description(buffer);
        }  
}

void Execute(Address old_PC, int opcode, INST inst)
{
    /* zero the opcode name and instruction descruiption */
    clear_opcode();
    clear_instruction_description();

    switch(opcode)
        {
        case 0: /* Non-register, Non-memory Instructions */
            non_register_non_memory_instruction(inst);
            time = time + 1;
            break;

        case 1: /* Register Memory Reference Instructions */
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            register_memory_reference_instructions(inst, opcode, old_PC);
            time = time + 2;
            break;
            
        case 10: /* IOT -- Input/Output Transfer  */
            {
                Word *reg = get_register((inst >> 10) & 0x3);
                char *regc = get_register_name((inst >> 10) & 0x3);
                char buffer[20];

                int device = (inst & MASK_IOT_DEVICE) >> 3;
                int function = (inst & MASK_IOT_FUNCTION);
                /* check for device = 3 -- Input */
                if (device == 3)
                    {
                        append_opcode("IOT 3");
                        *reg = getc(stdin) & MASK_W16;
                        sprintf(buffer, "0x%04X -> %s", *reg, regc);
                        append_instruction_description(buffer);
                    }
                /* or device = 4 -- Output */
                else if (device == 4)
                    {
                        append_opcode("IOT 4");
                        putc((*reg & 0xFF), stdout);
                        sprintf(buffer, "%s -> 0x%04X", regc, *reg);
                        append_instruction_description(buffer);
                    }
                else
                    {
                        append_opcode("IOT <bad-device>");
                        fprintf(stderr, "IOT function %d to unknown device %d; halting\n", function, device);
                        Halted = TRUE;
                    }
            
                time = time + 1;
                break;
            }

        case 11: /* Non-register Memory Reference Instructions */
            non_register_memory_reference_instruction(old_PC, inst);
            time = time + 1;
            break;

        case 12: /* Push and Pop stack operations */
            stack_operation_instruction(old_PC, inst);
            time = time + 3;
            break;

        case 14: /* Register-to-Register Instructions */
            register_to_register_instruction(inst);
            time = time + 1;
            break;

        case 15: /* Non-memory Register Instructions */
            non_memory_register_instruction(inst);
            time = time + 1;
            break;
        }

    if (verbose)
    fprintf(stderr, "Time %3lld: PC=0x%04X instruction = 0x%04X (%s)%s\n", 
            time, old_PC, inst, get_opcode(), get_instruction_description());
}


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

void Interpreter(STRING name)
{
    Load_Object_File(name);

    PSW = 1;

    while (!Halted)
        {
            Address old_PC = PC;
            INST inst = Fetch_Memory(PC);
            PC = (PC + 1) & MASK_W16;
            int opcode = Decode_Instruction(inst);
            Execute(old_PC, opcode, inst);
        }
}


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

void scanargs(STRING s)
{
    /* check each character of the option list for
       its meaning. */

    while (*++s != '\0')
        switch (*s)
            {

            case 'D': /* debug option */
                debug = TRUE;

            case 'b': /* binary object file input */
                ASCII = FALSE;
                break;

            case 'a': /* ASCII object file input */
                ASCII = TRUE;
                break;

            case 'v': /* verbose option */
                verbose = !verbose;
                break;

            case 's': /* switch register setting */
            case 'S': /* switch register setting */
                Switch_Register = hex_value(&s[1]) & MASK_W16;
                if (debug) fprintf(stderr, "Switch Register is 0x%03X\n", Switch_Register);
                break;

            default:
                fprintf (stderr,"pdp8: Bad option %c\n", *s);
                fprintf (stderr,"usage: pdp8 [-D] file\n");
                exit(1);
            }
}



/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

int main(int argc, STRING *argv)
{
    Boolean filenamenotgiven = TRUE;

    /* main driver program.  Define the input file
       from either standard input or a name on the
       command line.  Process all arguments. */

    while (argc > 1)
        {
            argc--, argv++;
            if (**argv == '-')
                scanargs(*argv);
            else
                {
                    filenamenotgiven = FALSE;
                    input = fopen(*argv,"r");
                    if (input == NULL)
                        {
                            fprintf (stderr, "Can't open %s\n",*argv);
                        }
                    else
                        {
                            Interpreter(*argv);
                            fclose(input);
                        }
                }
        }

    if (filenamenotgiven)
        {
            input = stdin;
            Interpreter(NULL);
        }

    exit(0);
}
