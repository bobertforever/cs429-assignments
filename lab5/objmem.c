/* 
   Assembler for PDP-8.  Memory and object file creation. 
*/

#include "asm8.h"


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

/* we want to assemble instructions.  We could assemble and output them
   all at once.  But we have a problem of forward references.  So we
   keep the (partially) assembled instructions in an array of them,
   essentially simulated memory.  That allows us to come back and 
   fix them up when the forward reference is resolved.

   We need to know which memory locations are from assembled
   instructions, and which are just empty; so each memory location
   has a bit (defined/not defined).
*/

INST     memory[4096];
Boolean defined[4096];
Address entry_point = 0;


void Clear_Object_Code(void)
{
    int i;
    for (i = 0; i < 4096; i++)
        {
            defined[i] = FALSE;
        }
}

void Define_Object_Code(Address addr, INST inst, Boolean redefine)
{
    if (debug)
        fprintf(stderr, "object code: 0x%03X = 0x%03X\n", addr, inst);
    if (defined[addr] && !redefine)
        {
            fprintf(stderr, "redefined memory location: 0x%03X: was 0x%03X; new value 0x%03X\n",
                    addr, memory[addr], inst);
            number_of_errors += 1;
        }
                
    defined[addr] = TRUE;
    memory[addr] = inst;
}

INST Fetch_Object_Code(Address addr)
{
    INST inst;

    if (defined[addr])
        inst = memory[addr];
    else
        inst = 0;

    if (debug)
        fprintf(stderr, "read object code: 0x%03X = 0x%03X\n", addr, inst);
    return(inst);
}


void Output_Object_Code(void)
{
    // Write the magic number
    char buffer[] = {'O','B','J','8'};
    fwrite(buffer, sizeof(char), sizeof(buffer), output);

    // Write the entry point
    int tmp = entry_point >> 6;
    fwrite(&tmp, 1, 1, output);
    tmp = entry_point & 0x3F;
    fwrite(&tmp, 1, 1, output);
    
    // Write the blocks
    int i = 0;
    while(i < 4096)
        {
            while(!defined[i] && i < 4096)
                i++;

            int j = i;
            int block_len = 0;
            while(defined[j] && j < 4096)
                {
                    block_len++;
                    j++;
                }

            if(block_len != 0)
                {
                    // We can only print 126 blocks at a time
                    block_len = block_len > 126 ? 126 : block_len;

                    tmp = ((block_len * 2) + 3) & 0xFF;
                    fwrite(&tmp, 1, 1, output);

                    tmp = i >> 6;
                    fwrite(&tmp, 1, 1, output);
                    tmp = i & 0x3F;
                    fwrite(&tmp, 1, 1, output);

                    for(j = 0; j < block_len; j++)
                        {
                            tmp = memory[i + j] >> 6;
                            fwrite(&tmp, 1, 1, output);
                            tmp = memory[i + j] & 0x3F;
                            fwrite(&tmp, 1, 1, output);
                        }    
                    i += block_len;
                }
        }
}


