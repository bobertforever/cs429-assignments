CS429 Computer Architecture assignments
=================

UT CS429 assignments from Fall of 2013 under Dr. Peterson.

### Lab1 - Bit twiddling
Introductory assignment to C and bit operations.

### Lab2 - A 5-bit encoder/decoder 
A program to translate from a binary Linux file into just "A-Z" and "0-5" and back.

### Lab3 - Facts and Questions
A program to read a set of facts and remember them, then read a set of questions, look up the answers, and print them.

### Lab4 - PDP-8
A program to simulate a PDP-8 computer system.

### Lab5 - PDP-8 Assembler and Object File Format
A  PDP-8 assembler.

### Lab6 - Victim Cache
A cache simulator that takes a description of a set of caches (in the description file) and a trace file (of memory references), and computes the number of hits and misses for the described caches for the given memory trace.

### Lab7 - PDP-429
An extension of Lab4 to make the PDP-8 16-bit.
