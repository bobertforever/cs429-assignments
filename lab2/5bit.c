/* 5bit.c
 * Robert Lynch
 * rml953
 *
 * This program takes any given file and encodes it by taking sections of 5-bits,
 * assigning them new values (From A-Z/ 0-5), and writing it to stdout.
 */

#include <stdio.h>
#include <stdlib.h>

typedef short Boolean;
#define TRUE 1
#define FALSE 0

//Decode input file
Boolean decode = FALSE;
FILE *file;

void scanargs(char *s) {
    while (*s != '\0') {
        switch (*s++) {
            case '-': /* option indicator */
                break;

            case 'd': /* decode input file */
                decode = TRUE;
                break;

            default:
                fprintf(stderr, "5bit: Bad option %c\n", s[-1]);
                fprintf(stderr, "       -d -- decode input file\n");
                exit(1);
        }
    }
}

char encodeChar (int num) {
    if (num >= 0 && num < 26) {
        return (char) num + 'A';
    } else if (num >= 26 && num <= 31) {
        return (char) (num - 26) + '0';
    } else {
        return '!';
    }
}

char decodeChar (char ch) {
    if (ch >= 'A' && ch <= 'Z') {
        return ch - 'A';
    } else {
        return (ch + 26) - '0';
    }
}

void encodeFile (void) {
    int c = 0;
    int buffer = 0;
    int bufferSize = 0;
    int charInLine = 0;

    // Go through the file one byte at a time
    while ((c = fgetc((!file) ? stdin : file)) != EOF) {
        printf("%c", encodeChar(((buffer | (c >> bufferSize)) & 0xF8) >> 3));
        charInLine++;
        buffer = c << (5 - bufferSize);
        bufferSize += 3;

        // If the buffer contains more then 5 bits, dump a character out
        if(bufferSize >= 5) {
            printf("%c", encodeChar((buffer & 0xF8) >> 3));
            charInLine++;
            buffer <<= 5;
            bufferSize -= 5;
        }

        // Print a newline evcery 72 characters
        if (charInLine == 72) {
            printf("\n");
            charInLine = 0;
        }
    }
    
    // Dump the rest of the buffer
    if (bufferSize != 0) {
        printf("%c", encodeChar((buffer & 0xF8) >> 3));
    }

    // Print a newline if we didn't just print one
    if (charInLine != 0) {
        printf("\n");
    }

    fclose((!file) ? stdin : file);
}

void decodeFile (void) {
    char c = 0;
    char buffer = 0;
    int bufferSize = 0;

    while((c = fgetc((!file) ? stdin : file)) != EOF) {
        // Don't deal with the newlines we inserted
        if ( c != '\n') {

            // Decode the next char and add it to the buffer
            buffer |= ((decodeChar(c) << 3) >> bufferSize);
            bufferSize += 5;

            // Once the buffer is full, print out a byte
            if(bufferSize >= 8) {
                printf("%c", buffer);
                bufferSize -= 8;
                buffer = (decodeChar(c) << 8 - bufferSize);
            }
        }
    }

    fclose((!file) ? stdin : file);
}

int main (int argc, char **argv) {
 
    // Read args and perform encode/ decode
    while (argc > 1) {
        argc--, argv++;
        if (**argv == '-') {
            scanargs(*argv);
        } else {
            file = fopen(*argv, "rb");
            if (!file) {
                fprintf(stderr, "5bit: Cannot open file %s\n", *argv);
                exit(1);
            }
        }
    }

    if(decode) {
        decodeFile();
    } else {
        encodeFile();
    }

    return 0;
}
