/*
 * facts.c
 * Robert Lynch
 * rml953
 *
 * This program reads in a data file, arg 1, and parses it into a linked list,
 * which it uses to answer questions from a 2nd file, or stdin
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef short Boolean;
#define TRUE 1
#define FALSE 0

FILE *facts;
FILE *questions;
Boolean readFromStdin = FALSE;

// struct to hold the list of facts for a certain thing in a linked list
struct node {
    struct node *next;
    struct fact *facts;
    char *name;
};

// struct to hold the fact and value in a linked list
struct fact {
    struct fact *next;
    char *type;
    char *value;
};

// first node in the linked list
struct node *firstNode;

// retrieve a node by name
struct node* getNode(char *name) {
    struct node *currentNode = firstNode;
    while(currentNode != NULL) {
        if(strcmp(currentNode->name, name) == 0) {
            return currentNode;
        }
        currentNode = currentNode->next;
    }
    return NULL;
}

// retrieve a fact by name and node
struct fact* getFact(char *type, struct node *currentNode) {
    struct fact *currentFact = currentNode->facts;
    while(currentFact != NULL) {
        if(strcmp(currentFact->type, type) == 0) {
            return currentFact;
        }
        currentFact = currentFact->next;
    }
    return NULL;
}

// free all nodes, facts, and close files
void cleanup(void) {
    struct node *currentNode = firstNode;
    struct node *tempNode;
    struct fact *currentFact;
    struct fact *tempFact;
    
    // Loop through all nodes, free all facts, then free all nodes
    while(currentNode != NULL) {
        currentFact = currentNode->facts;
        while(currentFact != NULL) {
            tempFact = currentFact->next;
            free(currentFact->type);
            free(currentFact->value);
            free(currentFact);
            currentFact = tempFact;
        }
        tempNode = currentNode->next;
        free(currentNode->name);
        free(currentNode);
        currentNode = tempNode;
    }

    // Close the files
    fclose(facts);

    if(questions)
        fclose(questions);
}

// remove whitespace and delimiters
char* trim(char *s) {
    while(isspace(s[strlen(s) - 1]) || s[strlen(s) - 1] == ':') s[strlen(s) - 1] = '\0';
    while(isspace(*s) || *s == ':' || *s == '=') memmove(s, s + 1, strlen(s));
    return s;
}

void process(void) {
    // Variables for reading lines
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int n = 0;

    // Variables for parsing lines
    char fq = '0';
    char *type;
    char *fname;
    char *fvalue;

    // Variables for saving data
    struct node *currentNode;
    struct fact *currentFact;
    
    // Parse the fact file
    while ((read = getline(&line, &len, facts)) != -1) {
        if (*line != '\n') {
            // Allocate enough space for the information
            type = malloc(strlen(line));
            fname = malloc(strlen(line));
            fvalue = malloc(strlen(line));

            // Check for out of memory
            if(type == NULL || fname == NULL || fvalue == NULL) {
                fprintf(stderr, "Out of memory, quitting\n");
                cleanup();

                if(type)
                    free(type);
                if(fname)
                    free(fname);
                if(fvalue)
                    free(fvalue);

                exit(1);
            }

            // Read the line in and store it into variables
            n = sscanf(line, "%c %s %[^'='] %[^'\n']", &fq, type, fname, fvalue);

            // Remove leading/ trailing characters and whitespace
            type = trim(type);
            fname = trim(fname);
            fvalue = trim(fvalue);

            // Store the data if what we read was valid
            if(n == 4 && fq == 'F') {
                currentNode = getNode(type);
                if(currentNode == NULL) {
                    currentNode = malloc(sizeof(struct node));
                    currentNode->name = type;
                    currentNode->next = firstNode;
                    firstNode = currentNode;
                } else {
                    free(type);
                }
                
                currentFact = getFact(fname, currentNode);
                if(currentFact == NULL) {
                    currentFact = malloc(sizeof(struct fact));
                    currentFact->type = fname;
                    currentFact->next = currentNode->facts;
                    currentNode->facts = currentFact;
                } else {
                    free(fname);
                    free(currentFact->value);
                }
                currentFact->value = fvalue;
            } else {
                // Free the variables if the fact file was invalid format and we couldn't successfully read
                if(type)
                    free(type);
                if(fname)
                    free(fname);
                if(fvalue)
                    free(fvalue);
            }
        }
    }

    // Parse the questions and display the output
    while ((read = getline(&line, &len, readFromStdin ? stdin : questions)) != -1) {
        if(*line != '\n') {
            type = malloc(strlen(line));
            fname = malloc(strlen(line));

            // Check for out of memory
            if(type == NULL || fname == NULL) {
                fprintf(stderr, "Out of memory, quitting\n");
                cleanup();

                if(type)
                    free(type);
                if(fname)
                    free(fname);

                exit(1);
            }

            n = sscanf(line, "%c %s %s", &fq, type, fname);

            // If the line was parsed correctly, display the answers
            if(n == 3 && fq == 'Q') {
                type = trim(type);
                fname = trim(fname);

                currentNode = getNode(type);
                
                // If there is a node and fact with the correct value, display it
                if(currentNode != NULL) {
                    currentFact = getFact(fname, currentNode);
                    if(currentFact != NULL) {
                        printf("F %s: %s=%s\n", currentNode->name, currentFact->type, currentFact->value);
                    } else {
                        printf("F %s: %s=unknown\n", type, fname);
                    }
                } else {
                    printf("F %s: %s=unknown\n", type, fname);
                }
            }

            if(type)
                free(type);
            if(fname)
                free(fname);
        }
    }

    if(line) {
        free(line);
    }
}

int main (int argc, char **argv) {
    if (argc == 2) {
        // We'll be reading the questions from stdin
        argv++;
        facts = fopen(*argv, "r");
        if(!facts) {
            fprintf(stderr, "facts: error opening fact file %s\n", *argv);
            exit(1);
        }

        readFromStdin = TRUE;
    } else if (argc == 3) {
        // Read questinos from file
        argv++;
        facts = fopen(*argv, "r");
        if(!facts) {
            fprintf(stderr, "facts: error opening fact file %s\n", *argv);
            exit(1);
        }

        argv++;
        questions = fopen(*argv, "r");
        if(!questions) {
            fprintf(stderr, "facts: error opening question file %s\n", *argv);
            exit(1);
        }
    } else {
        fprintf(stderr, "facts: invalid number of arguments");
        exit(1);
    }

    process();

    cleanup();

    return 0;
}
